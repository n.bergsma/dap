FROM microsoft/dotnet:sdk AS build-env
WORKDIR /app

COPY . ./

RUN cd api
RUN dotnet restore
RUN dotnet publish -c Release -o out

FROM microsoft/dotnet:2.2.0-runtime-alpine3.8 as run-env
WORKDIR /app

RUN mkdir /data && chmod +rw /data

COPY --from=build-env /app/api/data /data
COPY --from=build-env /app/api/out .

RUN adduser -D web
USER web

CMD export PORT=$PORT && dotnet api.dll 


