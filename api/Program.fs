﻿open System
open System.Threading
open Suave
open Suave.Filters
open Suave.Operators
open Suave.Successful
open Suave.RequestErrors
open Suave.Cookie
open System.Net
open JWT
open JWT.Builder
open JWT.Algorithms
open JWT.Serializers
open Microsoft.FSharpLu.Json
open ServiceStack.DataAnnotations
open ServiceStack.OrmLite
open System.Text
open System.Linq
open System.IO
open System.Reflection

let hashPassword = BCrypt.Net.BCrypt.HashPassword
let passwordEqual hash pw = BCrypt.Net.BCrypt.Verify (pw, hash)
let secret = System.Environment.GetEnvironmentVariable("JWT_SECRET") // 
let connections = OrmLiteConnectionFactory ("/data/users.sqlite", SqliteDialect.Provider)
let assembly = (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).AbsolutePath
let storage = connections.Open()

let utf8 = UTF8Encoding.UTF8

[<Alias("users")>]
type User = {
    [<Required>][<StringLength(100)>]
    mutable name: string
    
    [<PrimaryKey>][<Required>][<StringLength(256)>][<Alias("email_address")>] 
    mutable emailAddress: string 
    
    [<Required>][<StringLength(128)>][<Alias("password_hash")>]
    mutable passwordHash: string
    
    [<Required>][<Alias("is_director")>]
    mutable isDirector: bool
    
    [<Required>][<Alias("is_administrator")>] 
    mutable isAdministrator: bool
    
    [<Required>][<Alias("is_care_giver")>]
    mutable isCareGiver: bool
}

let makeToken user = 
    JwtBuilder().
        WithAlgorithm(HMACSHA256Algorithm()).
        WithSecret(secret).
        AddClaim("exp", DateTimeOffset.UtcNow.AddMonths(1).ToUnixTimeMilliseconds()).
        AddClaim("name", user.name).
        AddClaim("email", user.emailAddress).
        AddClaim("isDirector", user.isDirector).
        AddClaim("isAdministrator", user.isAdministrator).
        AddClaim("isCareGiver", user.isCareGiver).
        Build()



let config  = 
    let port = System.Environment.GetEnvironmentVariable("PORT")
    let ipZero = IPAddress.Parse("0.0.0.0")

    { defaultConfig with 
        bindings = [(HttpBinding.create HTTP ipZero (uint16 port))] 
        cancellationToken = Async.DefaultCancellationToken
        compressedFilesFolder = Some "/tmp"
        homeFolder = Some ((Path.GetDirectoryName assembly) + "/www")
    }

let setAuthCookie profile =
    Cookie.setCookie { HttpCookie.empty with name = "auth"; value = makeToken profile; httpOnly=true; sameSite = Some(SameSite.Strict) }
        
let unsetAuthCookie = 
    Cookie.unsetCookie "auth"
    
type Role = Director | Administrator | CareGiver



type Claims = {
    name: string
    email: string
    isDirector: bool
    isAdministrator: bool
    isCareGiver: bool
} with
    member this.roles =
        seq {
            if this.isDirector then yield Director
            if this.isAdministrator then yield Administrator
            if this.isCareGiver then yield CareGiver
        } 

let claimsFromCookie cookie = 
    try
        Some(JwtBuilder().
            WithSecret(secret).
            MustVerifySignature().
            Decode<Claims>(cookie.value))
    with
        | _ -> None
        
let claimsFromUser (u: User) : Claims = 
    { name = u.name; email = u.emailAddress; isDirector = u.isDirector; isAdministrator = u.isAdministrator; isCareGiver = u.isCareGiver }

let toJson v =
  Compact.serialize(v) |> OK >=> Writers.setMimeType "application/json; charset=utf-8" 


let inline fromJson< ^T> s = 
    try 
        Ok (Compact.deserialize<'T> s) 
    with
        | ex -> Error ex.Message


type Credentials = { 
        username : string
        password : string
    } 
    
type ErrorResponse = {
    error : string
}

type SuccessResponse = {
    success : bool
}

let error code msg = 
    code ({ error = msg } |> Compact.serialize) >=> Writers.setMimeType "application/json; charset=utf-8"
    
let success = 
    OK ({ success = true } |> Compact.serialize) >=> Writers.setMimeType "application/json; charset=utf-8"

let authenticate username password = 
    let profiles = List.ofSeq (storage.Select<User>(fun p -> p.emailAddress = username))
    match profiles with
        | p::_ when passwordEqual p.passwordHash password -> Some(p)
        | _ -> None

let createSession =
    context(fun ctx ->
        let body = utf8.GetString ctx.request.rawForm
        let credentials = fromJson<Credentials> body
        
        match credentials with
            | Ok c -> match authenticate c.username c.password with
                | Some p -> setAuthCookie p >=> (claimsFromUser p |> toJson)
                | None -> unsetAuthCookie >=> error UNAUTHORIZED "could not authenticate user" 
            | Error e  -> unsetAuthCookie >=> error UNAUTHORIZED "could not authenticate user"
        )

let endSession = unsetAuthCookie >=> success

let optionFold map opt = match opt with
    | Some v -> map v
    | None -> None

let allowed roles (claims: Claims) = 
    Enumerable.Intersect (roles, claims.roles) |> Enumerable.Any

let authorize roles next = context(fun ctx ->
    let claims = ctx.request.cookies |> Map.tryFind "auth" |> optionFold claimsFromCookie
    match claims with
        | Some c when allowed roles c -> Writers.setUserData "claims" claims >=> next
        | _ -> error FORBIDDEN "unauthorised"
)

let getUsersMe = context(fun ctx ->
    let claims = ctx.userState |> Map.tryFind "claims"
    match claims with 
        | Some c -> c |> toJson
        | None -> error NOT_FOUND "resource not found"
)

[<EntryPoint>]
let main argv =    
    let routes =
      choose
        [ GET >=> choose
            [ path "/api" >=> OK "yes, you've come to the right place"
              path "/api/users/me" >=> authorize [Director;Administrator;CareGiver] getUsersMe ]
          POST >=> choose
            [ path "/api/session" >=> createSession ]
          DELETE >=> choose
            [ path "/api/session" >=> endSession ]
          
          //static file handling
          GET >=> path "/" >=> Files.browseFileHome "index.htm"
          GET >=> Files.browseHome
          
          //fallback
          NOT_FOUND "resource not found"]

    printfn "service started"
    startWebServer config routes

    0 